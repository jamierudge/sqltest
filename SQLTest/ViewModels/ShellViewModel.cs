using SQLTest.ViewModels;

namespace SQLTest
{
    public class ShellViewModel : IShell
    {
        public DebugViewModel DebugViewModel { get; set; }

        public ShellViewModel(DebugViewModel debugViewModel)
        {
            DebugViewModel = debugViewModel;
        }
    }
}