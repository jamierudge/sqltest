﻿using Caliburn.Micro;
using SQLTest.SQL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Media;
using System.Windows.Threading;

namespace SQLTest.ViewModels
{
    public enum SortOption
    {
        Level,
        Timestamp,
        Message
    }

    public class DebugViewModel : PropertyChangedBase, IHandle<MouseWheelEvent>
    {
        SQLService m_sqlService;

        IEventAggregator m_eventAggregator;

        ObservableCollection<LogInfoModel> m_logInfos;

        List<string> m_filterList;

        System.Timers.Timer m_timer;

        int m_mouseDelta = 0;
        int m_collectionIndex = 0;

        SortOption m_sortOption;

        // Stores all the debug information stored in the table
        public ObservableCollection<LogInfoModel> Info
        {
            get { return m_logInfos; }
            set
            {
                m_logInfos = value;
                NotifyOfPropertyChange(() => Info);
            }
        }

        string m_filterString;

        public string FilterString
        {
            get { return m_filterString; }
            set
            {
                m_filterString = value;
                NotifyOfPropertyChange(() => FilterString);
                ReassignToLogInfoCollection();
            }
        }

        #region FilterEnablingProperties

        bool m_debugEnabled = true;
        bool m_warnEnabled = true;
        bool m_errorsEnabled = true;
        bool m_fatalsEnabled = true;
        bool m_infoEnabled = true;
        bool m_tracesEnabled = true;

        public bool DebugEnabled
        {
            get { return m_debugEnabled; }
            set
            {
                m_debugEnabled = value;
                NotifyOfPropertyChange(() => DebugEnabled);

                CheckFilter();
            }
        }

        public bool WarnEnabled
        {
            get { return m_warnEnabled; }
            set
            {
                m_warnEnabled = value;
                NotifyOfPropertyChange(() => WarnEnabled);

                CheckFilter();
            }
        }

        public bool ErrorsEnabled
        {
            get { return m_errorsEnabled; }
            set
            {
                m_errorsEnabled = value;
                NotifyOfPropertyChange(() => ErrorsEnabled);

                CheckFilter();
            }
        }

        public bool FatalsEnabled
        {
            get { return m_fatalsEnabled; }
            set
            {
                m_fatalsEnabled = value;
                NotifyOfPropertyChange(() => FatalsEnabled);

                CheckFilter();
            }
        }

        public bool InfoEnabled
        {
            get { return m_infoEnabled; }
            set
            {
                m_infoEnabled = value;
                NotifyOfPropertyChange(() => InfoEnabled);

                CheckFilter();
            }
        }

        public bool TracesEnabled
        {
            get { return m_tracesEnabled; }
            set
            {
                m_tracesEnabled = value;
                NotifyOfPropertyChange(() => TracesEnabled);
                CheckFilter();
            }
        }

        #endregion

        #region FilterCountProperties

        string m_debugsCount;
        string m_warnCount;
        string m_errorsCount;
        string m_fatalsCount;
        string m_infosCount;
        string m_tracesCount;

        public string DebugsCount
        {
            get { return m_debugsCount; }
            set
            {
                m_debugsCount = value;
                NotifyOfPropertyChange(() => DebugsCount);
            }
        }

        public string WarnCount
        {
            get { return m_warnCount; }
            set
            {
                m_warnCount = value;
                NotifyOfPropertyChange(() => WarnCount);
            }
        }

        public string ErrorsCount
        {
            get { return m_errorsCount; }
            set
            {
                m_errorsCount = value;
                NotifyOfPropertyChange(() => ErrorsCount);
            }
        }

        public string FatalsCount
        {
            get { return m_fatalsCount; }
            set
            {
                m_fatalsCount = value;
                NotifyOfPropertyChange(() => FatalsCount);
            }
        }

        public string InfosCount
        {
            get { return m_infosCount; }
            set
            {
                m_infosCount = value;
                NotifyOfPropertyChange(() => InfosCount);
            }
        }

        public string TracesCount
        {
            get { return m_tracesCount; }
            set
            {
                m_tracesCount = value;
                NotifyOfPropertyChange(() => TracesCount);
            }
        }

        #endregion

        public DebugViewModel(SQLService sqlService)
        {
            m_eventAggregator = IoC.Get<IEventAggregator>();

            m_eventAggregator.Subscribe(this);

            m_sqlService = sqlService;

            m_filterString = String.Empty;

            // Assign filter names for each level of debug log
            m_filterList = new List<string>(new string[] { "DEBUG", "WARN", "ERROR", "FATAL", "INFO", "TRACE" });

            Info = new ObservableCollection<LogInfoModel>(); 

            for (int i = 0; i < 30; i++)
            {
                Info.Add(m_sqlService.GetLogInfoData(i));
            }

            m_totalCount = UpdateLevelCount();

            m_timer = new System.Timers.Timer();
            m_timer.Interval = 1000;
            m_timer.Elapsed += Elapsed;

            m_timer.AutoReset = true;
            m_timer.Start();
        }

        void Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

            if (dispatcher == null)
            {
                if (App.Current != null)
                {
                    if (App.Current.Dispatcher != null)
                    {
                        App.Current.Dispatcher.Invoke(() => UpdateFilter());
                    }
                }
            }
            else
            {
                UpdateFilter();
            }
        }

        public void UpdateFilter()
        {
            m_sqlService.Count = 0;

            if (int.Parse(m_sqlService.GetRowCount("DEBUG")) +
               int.Parse(m_sqlService.GetRowCount("WARN")) +
               int.Parse(m_sqlService.GetRowCount("ERROR")) +
               int.Parse(m_sqlService.GetRowCount("FATAL")) +
               int.Parse(m_sqlService.GetRowCount("INFO")) +
               int.Parse(m_sqlService.GetRowCount("TRACE")) > m_totalCount)
            {
                UpdateLevelCount();
            }
        }

        void CheckFilter()
        {
            if (m_filterList == null)
                return;

            if (DebugEnabled)
            {
                if (!m_filterList.Contains("DEBUG"))
                    m_filterList.Add("DEBUG");
            }
            else
            {
                if (m_filterList.Contains("DEBUG"))
                    m_filterList.Remove("DEBUG");
            }

            if (WarnEnabled)
            {
                if (!m_filterList.Contains("WARN"))
                    m_filterList.Add("WARN");
            }
            else
            {
                if (m_filterList.Contains("WARN"))
                    m_filterList.Remove("WARN");
            }

            if (ErrorsEnabled)
            {
                if (!m_filterList.Contains("ERROR"))
                    m_filterList.Add("ERROR");
            }
            else
            {
                if (m_filterList.Contains("ERROR"))
                    m_filterList.Remove("ERROR");
            }

            if (FatalsEnabled)
            {
                if (!m_filterList.Contains("FATAL"))
                    m_filterList.Add("FATAL");
            }
            else
            {
                if (m_filterList.Contains("FATAL"))
                    m_filterList.Remove("FATAL");
            }

            if (InfoEnabled)
            {
                if (!m_filterList.Contains("INFO"))
                    m_filterList.Add("INFO");
            }
            else
            {
                if (m_filterList.Contains("INFO"))
                    m_filterList.Remove("INFO");
            }

            if (TracesEnabled)
            {
                if (!m_filterList.Contains("TRACE"))
                    m_filterList.Add("TRACE");
            }
            else
            {
                if (m_filterList.Contains("TRACE"))
                    m_filterList.Remove("TRACE");
            }

            ReassignToLogInfoCollection();
        }

        int m_totalCount;

        int UpdateLevelCount()
        {
            DebugsCount = m_sqlService.GetRowCount("DEBUG") + " - DEBUGS";
            WarnCount = m_sqlService.GetRowCount("WARN") + " - WARN";
            ErrorsCount = m_sqlService.GetRowCount("ERROR") + " - ERRORS";
            FatalsCount = m_sqlService.GetRowCount("FATAL") + " - FATALS";
            InfosCount = m_sqlService.GetRowCount("INFO") + " - INFO";
            TracesCount = m_sqlService.GetRowCount("TRACE") + " - TRACES";

            m_totalCount = Int32.Parse(m_sqlService.GetRowCount("DEBUG"));
            m_totalCount += Int32.Parse(m_sqlService.GetRowCount("WARN"));
            m_totalCount += Int32.Parse(m_sqlService.GetRowCount("ERROR"));
            m_totalCount += Int32.Parse(m_sqlService.GetRowCount("FATAL"));
            m_totalCount += Int32.Parse(m_sqlService.GetRowCount("INFO"));
            m_totalCount += Int32.Parse(m_sqlService.GetRowCount("TRACE"));

            return m_totalCount;
        }

        #region ColumnSortingProperties

        int m_sortOrder;

        int m_levelSort;
        int m_timestampSort;
        int m_messageSort;

        public int IsLevelSort
        {
            get { return m_levelSort; }
            set
            {
                m_levelSort = value;
                NotifyOfPropertyChange(() => IsLevelSort);
            }
        }

        public int IsTimestampSort
        {
            get { return m_timestampSort; }
            set
            {
                m_timestampSort = value;
                NotifyOfPropertyChange(() => IsTimestampSort);
            }
        }

        public int IsMessageSort
        {
            get { return m_messageSort; }
            set
            {
                m_messageSort = value;
                NotifyOfPropertyChange(() => IsMessageSort);
            }
        }

        #endregion

        public void SortColumn(SortOption column)
        {
            m_sortOrder++;

            if (m_sortOrder > 1)
                m_sortOrder = -1;

            m_sortOption = column;

            switch (column)
            {
                case SortOption.Level:
                    IsLevelSort = m_sortOrder;
                    IsTimestampSort = IsMessageSort = 0;
                    break;

                case SortOption.Timestamp:
                    IsTimestampSort = m_sortOrder;
                    IsLevelSort = IsMessageSort = 0;
                    break;

                case SortOption.Message:
                    IsMessageSort = m_sortOrder;
                    IsLevelSort = IsTimestampSort = 0;
                    break;
            }

            ReassignToLogInfoCollection();
        }

        void ReassignToLogInfoCollection()
        {
            int infoCount = Info.Count;

            if (m_sortOrder > 0)
            {
                for (int i = 0; i < infoCount; i++)
                {
                    Info.RemoveAt(0);
                    Info.Add(m_sqlService.GetLogInfoData(m_collectionIndex + i, FilterString, m_sortOption, false, m_filterList.ToArray()));
                }
            }
            else if (m_sortOrder < 0)
            {
                for (int i = 0; i < infoCount; i++)
                {
                    Info.RemoveAt(0);
                    Info.Add(m_sqlService.GetLogInfoData(m_collectionIndex + i, FilterString, m_sortOption, true, m_filterList.ToArray()));
                }
            }
            else
            {
                for (int i = 0; i < infoCount; i++)
                {
                    Info.RemoveAt(0);
                    Info.Add(m_sqlService.GetLogInfoData(m_collectionIndex + i, FilterString, m_filterList.ToArray()));
                }
            }
        }

        public void Handle(MouseWheelEvent message)
        {
            m_mouseDelta = -message.MouseDelta;

            m_collectionIndex += m_mouseDelta;

            if(m_collectionIndex < 0)
            {
                m_collectionIndex = 0;
            }

            ReassignToLogInfoCollection();

            m_mouseDelta = 0;
        }
    }
}
