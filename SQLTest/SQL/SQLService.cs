﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Diagnostics;
using SQLTest.ViewModels;

namespace SQLTest.SQL
{
    public class SQLService
    {
        readonly string m_connectionStr;
        readonly string m_fileName;

        const string TableName = "Log";

        // Keeps track of how many cells are in the table
        public int Count { get; set; }

        public SQLService()
        {
            m_fileName = $"{Environment.GetEnvironmentVariable("LocalAppData")}/Simworx/ControlMaster/Server/Logs/debug.db";
            m_connectionStr = $"DataSource={m_fileName};Version=3;";
        }

        // Creates a command to select data from a table without any conditions
        public LogInfoModel GetLogInfoData(int offset)
        {
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM {TableName} LIMIT 1 OFFSET {offset}");

            return ExecuteAndDisposeCommand(command);
        }

        // Creates a command to select data from a table based on the filters provided
        public LogInfoModel GetLogInfoData(int offset, params string[] filters)
        {
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM {TableName} WHERE ");

            ConstructCommandFromFilterList(command, filters);

            command.CommandText += $" LIMIT 1 OFFSET {offset}";

            return ExecuteAndDisposeCommand(command);
        }

        // Creates a command to select data from a table based on user input and the filters provided
        public LogInfoModel GetLogInfoData(int offset, string filter, params string[] filters)
        {
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM {TableName} WHERE ");

            ConstructCommandFromFilterList(command, filters);
            ConstructCommandFromString(command, filter);

            command.CommandText += $" LIMIT 1 OFFSET {offset}";

            return ExecuteAndDisposeCommand(command);
        }

        // Creates a command to select data from a table based on the above method conditions and sorts them in either ascending or descending order
        public LogInfoModel GetLogInfoData(int offset, string filter, SortOption column, bool orderByDescending, params string[] filters)
        {
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM {TableName} WHERE ");

            ConstructCommandFromFilterList(command, filters);
            ConstructCommandFromString(command, filter);

            if (orderByDescending)
            {
                SortByDescending(command, column);
            }
            else
            {
                SortByAscending(command, column);
            }

            command.CommandText += $" LIMIT 1 OFFSET {offset}";

            return ExecuteAndDisposeCommand(command);
        }

        private void ConstructCommandFromFilterList(SQLiteCommand command, params string[] filters)
        {
            // Select data from the database with the specified table name
            string[] parameters = new string[filters.Length];

            command.CommandText += "(";

            for (int i = 0; i < filters.Length; i++)
            {
                parameters[i] = $"@Filter{i}";
                command.Parameters.AddWithValue(parameters[i], filters[i]);

                command.CommandText += $"Level LIKE UPPER({parameters[i]})";

                if (i < filters.Length - 1)
                {
                    command.CommandText += " OR ";
                }
            }

            command.CommandText += ")";
        }

        private void ConstructCommandFromString(SQLiteCommand command, string filter)
        {
            if (filter.Length > 0)
            {
                string parameter = "@Search";
                command.CommandText += $" AND Timestamp LIKE UPPER({parameter}) OR Message LIKE UPPER({parameter})";
                command.Parameters.AddWithValue(parameter, $"%{filter}%"); 
            }
        }

        private void SortByAscending(SQLiteCommand command, SortOption column)
        {
            command.CommandText += $" ORDER BY {column.ToString()} ASC";
        }

        private void SortByDescending(SQLiteCommand command, SortOption column)
        {
            command.CommandText += $" ORDER BY {column.ToString()} DESC";
        }

        // Pulls data from a database and stores it in a List container; arguments are optional
        private LogInfoModel ExecuteAndDisposeCommand(SQLiteCommand command)
        {
            LogInfoModel LogInfo = new LogInfoModel();

            using (var sqlConnection = new SQLiteConnection(m_connectionStr))
            {
                sqlConnection.Open();

                using (command)
                {
                    command.Connection = sqlConnection;

                    using (SQLiteDataReader rdr = command.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                LogInfo.Level = rdr.GetString(0);
                                LogInfo.Timestamp = rdr.GetString(1);
                                LogInfo.Message = rdr.GetString(2);

                                Count++;
                            }
                        }
                    }
                }
            }

            return LogInfo;
        }

        public string GetRowCount(string filter)
        {
            string count = String.Empty;

            using (var sqlConnection = new SQLiteConnection(m_connectionStr))
            {
                sqlConnection.Open();

                using (SQLiteCommand command = new SQLiteCommand($"SELECT COUNT(*) AS cnt FROM {TableName} WHERE Level = '{filter}';", sqlConnection))
                {
                    using (SQLiteDataReader rdr = command.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                count = rdr["cnt"].ToString();
                            }
                        }
                        else
                        {
                            count = "0";
                        }
                    }
                }
            }

            return count;
        }
    }
}
