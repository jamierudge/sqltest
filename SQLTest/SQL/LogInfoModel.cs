﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTest.SQL
{
    public class LogInfoModel
    {
        public string Level { get; set; }
        public string Timestamp { get; set; }
        public string Message { get; set; }
    }
}
