﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTest.SQL
{
    public class LogInfoComparer : IComparer<LogInfoModel>
    {
        private List<string> m_logList;

        public LogInfoComparer(List<string> logList)
        {
            m_logList = logList;
        }

        public int Compare(LogInfoModel x, LogInfoModel y)
        {
            return m_logList.IndexOf(x.Level) - m_logList.IndexOf(y.Level);
        }
    }
}
