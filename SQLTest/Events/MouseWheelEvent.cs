﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTest
{
    public class MouseWheelEvent
    {
        public int MouseDelta { get; set; }

        public MouseWheelEvent(int mousePos)
        {
            MouseDelta = mousePos;
        }
    }
}
