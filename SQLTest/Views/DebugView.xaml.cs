﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Caliburn.Micro;

namespace SQLTest.Views
{
    /// <summary>
    /// Interaction logic for DebugView.xaml
    /// </summary>
    public partial class DebugView : UserControl
    {
        ScrollViewer ListViewScroller = null;

        IEventAggregator m_eventAggregator;

        const int DELTA_DIVIDER = 40;

        public DebugView()
        {
            InitializeComponent();

            m_eventAggregator = IoC.Get<IEventAggregator>();

            ((INotifyCollectionChanged)DebugList.Items).CollectionChanged += OnUpdatedList;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            ListViewScroller = GetVisualChild<ScrollViewer>(DebugList);
        }

        private void ScrollChanged(object sender, MouseWheelEventArgs e)
        {
            m_eventAggregator.PublishOnUIThread(new MouseWheelEvent((e.Delta / DELTA_DIVIDER)));
        }

        public T GetVisualChild<T>(UIElement parent) where T : UIElement
        {
            T child = null;

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);

            for(int i = 0; i < numVisuals; i++)
            {
                UIElement element = (UIElement)VisualTreeHelper.GetChild(parent, i);

                child = element as T;

                if (child == null)
                    child = GetVisualChild<T>(element);
                else
                    break;
            }

            return child;
        }

        private void OnUpdatedList(object sender, NotifyCollectionChangedEventArgs e)
        {
            //ScrollerToBottom();
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            //ScrollerToBottom();
        }

        void ScrollerToBottom()
        {
            ListViewScroller?.ScrollToBottom();
        }
    }
}
