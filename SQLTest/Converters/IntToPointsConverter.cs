﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SQLTest.Converters
{
    class IntToPointsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((int)value)
            {
                case -1:
                    return new PointCollection
                    {
                        new System.Windows.Point(2.5, 2.5),
                        new System.Windows.Point(10, 2.5),
                        new System.Windows.Point(6.25, 7.5)
                    };

                case 1:
                    return new PointCollection
                    {
                        new System.Windows.Point(6.25, 2.5),
                        new System.Windows.Point(2.5, 7.5),
                        new System.Windows.Point(10, 7.5)
                    };
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
